/*CUBE*/


let num     = 2
let getcube = num ** 3;
console.log(`The cube of ${num} is ${getcube}`);


/*Address*/
let address = ["256","Washington Ave NW", "California", "90011"]

let [lot, street, country, zip] = address;
console.log(`I live at ${lot} ${street}, ${country} ${zip} `);

/*Animal*/
let animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: "1075 kgs",
	long: "20 ft 3 in"
};

let {name,type,weight,long} = animal;
console.log(`${name} was a ${type}. He weighted at ${weight} with a measurement of ${long}.`);


/*Loop*/

let array= [1,2,3,4,5]
array.forEach(function(array) {
	console.log(`${array}`)
});

/*Reduce number*/

let reduceNumber= array.reduce((accumulator, currentValue) => accumulator + currentValue, 0);

console.log(reduceNumber);

/*Dog*/
class Dog {
	constructor(name,age,breed){
		this.name  = name;
		this.age = age;
		this.breed= breed;
	}
};
const myDog = new Dog();

myDog.name="Frankie",
myDog.age=5,
myDog.breed="Miniature Dachshund";

console.log(myDog);
